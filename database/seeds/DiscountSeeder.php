<?php

use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->insert([
            'discount_code' => 'OFF20',
            'discount_percentage' => 20
        ]);

        DB::table('discounts')->insert([
            'discount_code' => 'OFF25',
            'discount_percentage' => 25
            ]);
    }
}
