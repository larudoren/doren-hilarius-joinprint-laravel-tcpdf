<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discount;

class DiscountsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discount = Discount::where('discount_code', $request->discount_code)->first();

        if (!$discount) {
            return redirect()->route('product.cart')->withErrors('Invalid Discount Code');
        }

        session()->put('discount',[
            name => $discount->discount_code,
            discount => $discount->discount_percentage,
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
