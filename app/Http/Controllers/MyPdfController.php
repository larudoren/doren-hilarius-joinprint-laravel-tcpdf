<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PDF;

class MyPdfController extends Controller
{
    public function index()
    {
        PDF::SetTitle("TCPDF with SVG Images");
        PDF::AddPage();
        PDF::ImageSVG($file=public_path() .'/images/assets/twitter-seeklogo.svg', $x=140, $y=225, $w='', $h=50, $link='', $align='right', $palign='', $border=0, $fitonpage=true);


        PDF::AddPage();
        PDF::SetAlpha(0.1);
        PDF::Image(public_path() .'/images/assets/laptop-computer-icons.png', 80, 100, 60, 60, '', '', '', true, 72);
        PDF::Output('tcpdfsvg.pdf');
    }

    public function download() {
        PDF::SetTitle("TCPDF with SVG Images");
        PDF::AddPage();
        PDF::ImageSVG($file=public_path() .'/images/assets/twitter-seeklogo.svg', $x=140, $y=225, $w='', $h=50, $link='', $align='right', $palign='', $border=0, $fitonpage=true);


        PDF::AddPage();
        PDF::SetAlpha(0.1);
        PDF::Image(public_path() .'/images/assets/laptop-computer-icons.png', 80, 100, 60, 60, '', '', '', true, 72);
        PDF::Output('tcpdfsvg.pdf', 'D');
    }
}
