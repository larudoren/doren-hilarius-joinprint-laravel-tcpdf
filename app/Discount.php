<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public static function findByCode($code)
    {
        return $this->where('discount_code',$code)->first();
    }

    public function discountn($total)
    {
        return ($this->discount_percentage / 100) * $total;
    }
}
